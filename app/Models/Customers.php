<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';

    public function medicalExamining() {
        return $this->hasMany('App\Models\MedicalExamining', 'customer_id', 'id');
    }

    public function previousMedicalExam() {
        return $this->hasMany('App\Models\PreviousMedicalExam', 'customer_id', 'id');
    }

    public function create($request, $dateOfBirth) {
        $customer = new Customers;
        $customer->health_insurance_card = $request->health_insurance_card;
        $customer->name = $request->name;
        $customer->sex = $request->sex;
        $customer->birth = $dateOfBirth;
        $customer->address = $request->address;
        $customer->personal_id = $request->personal_id;
        $customer->phone = $request->phone;

        $customer->save();
        return $customer;
    }
    public function search($request) {
        $customer = Customers::query()
                            ->where('personal_id', 'LIKE', '%'.$request->personal_id.'%')
                            ->where('name', 'LIKE', '%'.$request->name.'%')
                            ->paginate(5);
        $customer->appends(['personal_id'=>$request->personal_id, 'name' => $request->name]);
        return $customer;
    }
    public function deleteCustomer($id) {
        $deleteCustomer = Customers::where('id', $id)->delete();
        return $deleteCustomer;
    }

    public function getOneElementById($id) {
        $customer = Customers::with('medicalExamining')->where('id', $id)->get()->first();
        // $customer = Customers::find(1)->medicalExamining()->where('id', $id)->get()->first();

        return $customer;
    }

    public function updateCustomer($request, $id, $dateOfBirth) {
        $customer = Customers::where('id', $id)->update([
            'health_insurance_card' => $request->health_insurance_card,
            'name' => $request->name,
            'sex' => $request->sex,
            'birth' => $dateOfBirth,
            'address' => $request->address,
            'phone' => $request->phone
        ]);

        return $customer;
    }
}
