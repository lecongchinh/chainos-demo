<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusPatients extends Model
{
    protected $table = 'status_patients';

    public function index() {
        $status_patients = StatusPatients::all();

        return $status_patients;
    }
    
}
