<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    protected $table = 'patients';

    public function index() {
        $patients = Patients::paginate(5);
        return $patients;
    }

    public function search($request) {
        $patients = Patients::query()
                            ->where('id', 'LIKE', '%'.$request->id.'%')
                            ->where('fullname', 'LIKE', '%'.$request->fullname.'%')
                            ->paginate(5);
        $patients->appends(['id'=>$request->id, 'fullname' => $request->fullname]);
        return $patients;
    }

    public function updateStatus($request, $id) {
        $status_patients = Patients::where('id', $id)->update(['status_patients_id'=>$request->id]);

        return $status_patients;
    }

    public function create($request, $age) {

        $patient = new Patients;
        $patient->status_patients_id = '1';
        $patient->fullname = $request->fullname;
        $patient->sex = $request->sex;
        $patient->age = $age;
        $patient->post_code = $request->post_code;
        $patient->phone = $request->phone;
        $patient->address = $request->address;
        $patient->save();

        return $patient;
    }
}
