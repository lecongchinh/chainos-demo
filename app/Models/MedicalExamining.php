<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicalExamining extends Model
{
    protected $table = 'medical_examining';

    // public function customers() {
    //     return hasOne('App\Models\Customers');
    // }

    public function getOneByCustomerId($id) {
        $medical = MedicalExamining::where('customer_id', $id)->get();

        return $medical;
    }

    public function getOneById($id) {
        $medical = MedicalExamining::where('id', $id)->get();

        return $medical;
    }

    public function createElement($request, $customer_id) {
        $medicalExamining = new MedicalExamining;

        $medicalExamining->customer_id = $customer_id;
        $medicalExamining->symptom = $request->symptom;
        $medicalExamining->indications_analysis = $request->indications_analysis;
        $medicalExamining->results_analysis = $request->results_analysis;
        $medicalExamining->patient_monitoring_situation = $request->patient_monitoring_situation;
        $medicalExamining->daily_medication = $request->daily_medication;

        $medicalExamining->save();
    }

    public function updateElement($request, $id) {
        MedicalExamining::where('id', $id)->update([
            'symptom' => $request->symptom,
            'indications_analysis' => $request->indications_analysis,
            'results_analysis' => $request->results_analysis,
            'patient_monitoring_situation' => $request->patient_monitoring_situation,
            'daily_medication' => $request->daily_medication
        ]);
    }

    public function deleteElement($id) {
        MedicalExamining::where('id', $id)->delete();
    }
}
