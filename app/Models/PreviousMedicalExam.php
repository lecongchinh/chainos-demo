<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreviousMedicalExam extends Model
{
    protected $table = 'previous_medical_exam';
    public function customers() {
        return $this->belongsTo('App\Models\Customers');
    }

    public function createElement($request) {
        $previousMedicalExam = new PreviousMedicalExam;
        $previousMedicalExam->customer_id = $request['customer_id'];
        $previousMedicalExam->medical_history = $request['medical_history'];
        $previousMedicalExam->symptom = $request['symptom'];

        $previousMedicalExam->save();
    }
}
