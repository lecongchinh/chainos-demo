<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customers;
use DB;
use App\Models\MedicalExamining;
use App\Http\Requests\CustomerRequest;

class CustomersController extends Controller
{
    public function __construct() {
        $customers = new Customers();
        $customerRequest = new CustomerRequest;

        $this->customers = $customers;
        $this->customerRequest = $customerRequest;
    }

    public function index(Request $request)
    {
        return view('customers', ['customers' => $this->customers->search($request), 'request' => $request]);
    }
    public function getCreate() {
        return view('customer-register');
    }

    public function postCreate(Request $request) {
        // dd($request);

        $this->customerRequest->validateCustomer($request);

        $dateOfBirth = "".$request->year."-".$request->month."-".$request->day;
        $request->request->set('date', $dateOfBirth);
        
        $this->customers->create($request, $dateOfBirth);

        return redirect('/admin/customers')->with('success', 'Create customer completed !');
    }

    public function deleteCustomer($id) {
        DB::beginTransaction();
        try{
            $this->customers->deleteCustomer($id);
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollBack();
            return redirect('/admin/customers')->with('danger', $e->getMessage());
        }

        return redirect('/admin/customers')->with('success', 'Delete customer completed!');

        // $this->customers->deleteCustomer($id);

        // return redirect('/admin/customers')->with('success', 'Delete customer completed!');
    }

    public function getEdit($id) {
        $customer = $this->customers->getOneElementById($id);

        return view('customer-edit', ['customer'=>$customer]);
    }
    public function postEdit(Request $request, $id) {

        // dd($request);

        $dateOfBirth = "".$request->year."-".$request->month."-".$request->day;
        $request->request->set('date', $dateOfBirth);

        $this->customerRequest->validateCustomer($request);

        $this->customers->updateCustomer($request, $id, $dateOfBirth);

        return redirect('/admin/customers')->with('success', 'Update customer completed!');
    }

    public function showDetailCustomer($id) {
        $customer = $this->customers->getOneElementById($id);
        // dd($customer->medicalExamining);
        return view('customer-detail', ['customer' => $customer]);
    }
}
