<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MedicalExamining;
use App\Http\Controllers\PreviousMedicalExamController;
use DB;
use App\Http\Requests\MedicalExaminingRequest;

class MedicalExaminingController extends Controller
{
    public function __construct() {
        $medicalExamining = new MedicalExamining;
        $previousMedicalExamController = new PreviousMedicalExamController();
        $medicalExaminingRequest = new MedicalExaminingRequest;

        $this->medicalExamining = $medicalExamining;
        $this->previousMedicalExamController = $previousMedicalExamController;
        $this->medicalExaminingRequest = $medicalExaminingRequest;
    }
    public function getById($id) {
        $medical = $this->medicalExamining->getOneById($id);

        return $medical;
    }

    public function create(Request $request, $customer_id) {
        $this->medicalExaminingRequest->validateMedicalExamining($request);

        $this->medicalExamining->createElement($request, $customer_id);

    }

    public function update(Request $request, $id) {
        $this->medicalExaminingRequest->validateMedicalExamining($request);
        $this->medicalExamining->updateElement($request, $id);
    }

    public function delete($id) {
        DB::beginTransaction();

        try {
            $medical = $this->medicalExamining->getOneById($id);
            // dd($medical);

            $request=array(
                'customer_id' => $medical[0]->customer_id,
                'medical_history' => $medical[0]->results_analysis,
                'symptom' => $medical[0]->symptom
            );

            // dd($request);


            $this->previousMedicalExamController->createElement($request);
            $this->medicalExamining->deleteElement($id);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            // return redirect('/admin/customers/')->with('danger', 'Delete fail!');
        }
        // return redirect()->route('customer-detail', ['id' => $id])->with('danger', 'Delete fail!');
    }
}
