<?php

namespace App\Http\Controllers\Clients;
use App\Models\Patients;
use App\Models\StatusPatients;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class InforPatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $patients = new Patients();
        $status_patients = new StatusPatients();

        $this->patients = $patients;
        $this->status_patients = $status_patients;
    }

    public function index(Request $request)
    {
        return view('clients.search-infor', ['patients' => $this->patients->search($request), 'status_patients' => $this->status_patients->index(), 'request' => $request]);
    }

    public function updateStatus(Request $request, $id) {
        $this->patients->updateStatus($request, $id);
    }
}
