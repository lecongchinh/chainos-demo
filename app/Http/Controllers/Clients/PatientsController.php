<?php

namespace App\Http\Controllers\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Patients;
use DateTime;

class PatientsController extends Controller
{

    public function __construct() {
        $patients = new Patients();

        $this->patients = $patients;
    }

    public function index() {
        return view('clients.patient-register');
    }

    public function create(Request $request) {

        $dateOfBirth = "".$request->year."-".$request->month."-".$request->day;
        $dateNow = date("Y-m-d");
        $diff= abs(strtotime($dateNow) - strtotime($dateOfBirth));
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        

        $age = $years." Năm ".$months." tháng ".$days." ngày ";

        $request->request->set('date', $dateOfBirth);

        $validaData = $request->validate([
            'fullname' => 'required|max:50',
            'sex' => 'required',
            'post_code' => 'required',
            'phone' => 'required|regex:/(0)[0-9]/',
            'address' => 'required',
            'date' => 'Required|Date'
        ]);

        $this->patients->create($request, $age);

        return redirect('/admin/search-infor')->with('success', 'Create patient completed !');
    }

    public function show($id) {
        return view('clients.patient-detail');
    }
}
