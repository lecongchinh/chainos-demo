<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PreviousMedicalExam;

class PreviousMedicalExamController extends Controller
{
    public function __construct() {
        $previousMedicalExam = new PreviousMedicalExam();
        $this->previousMedicalExam = $previousMedicalExam;
    }

    public function createElement($request) {
        $this->previousMedicalExam->createElement($request);
    }
}
