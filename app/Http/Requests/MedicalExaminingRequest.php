<?php 

namespace App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedicalExaminingRequest extends Controller{
    public function validateMedicalExamining($request) {
        $validatedData = [
            'symptom'=>'sometimes|required',
            'indications_analysis' => 'sometimes|required',
            'results_analysis' => 'sometimes|required',
            'patient_monitoring_situation' => 'sometimes|required',
            'daily_medication' => 'sometimes|required'
        ];

        $this->validate($request, $validatedData, $this->messageValidateMedicalExamining());

        return $validatedData;
    }

    public function messageValidateMedicalExamining() {
        return [
            'symptom.required' => 'Trường symptom không được để trống',
            'indications_analysis.required' => 'Trường indications_analysis tính không được để trống',
            'results_analysis.required' => 'Trường results_analysis không được để trống',
            'patient_monitoring_situation.required' => 'Trường patient_monitoring_situation không được để trống',
            'daily_medication.required' => 'Trường daily_medication không được để trống',
        ];
    }
}