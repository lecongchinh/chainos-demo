<?php 

namespace App\Http\Requests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerRequest extends Controller{
    public function validateCustomer($request) {
        $validatedData = [
            'name' => 'sometimes|required|max:50',
            'sex' => 'sometimes|required',
            'phone' => 'sometimes|required|regex:/(0)[0-9]/',
            'address' => 'sometimes|required',
            'date' => 'sometimes|Required|Date',
            'health_insurance_card' => 'sometimes|required',
            'personal_id' => 'sometimes|required|unique:customers'
        ];

        $this->validate($request, $validatedData, $this->messageValidateCustomer());

        return $validatedData;
    }

    public function messageValidateCustomer() {
        return [
            'name.required' => 'Trường Tên không được để trống',
            'sex.required' => 'Trường giới tính không được để trống',
            'phone.required' => 'Trường số điện thoại không được để trống',
            'phone.regex' => 'Bạn đã nhập sai định dạng số điện thoại',
            'address.required' => 'Trường địa chỉ không được để trống',
            'date.required' => 'Trường Date không được để trống',
            'health_insurance_card.required' => 'Trường Mã thẻ bảo hiểm y tế không được để trống',
            'personal_id.required' => 'Trường Personal ID không được để trống',
        ];
    }
}