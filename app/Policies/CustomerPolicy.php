<?php

namespace App\Policies;

use App\User;
use App\Models\Customers;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any customers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the customers.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Customers  $customers
     * @return mixed
     */
    public function view(User $user, Customers $customers)
    {
        //
    }

    /**
     * Determine whether the user can create customers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the customers.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Customers  $customers
     * @return mixed
     */
    public function update(User $user, Customers $customers)
    {
        //
    }

    /**
     * Determine whether the user can delete the customers.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Customers  $customers
     * @return mixed
     */
    public function delete(User $user, Customers $customers)
    {
        //
    }

    /**
     * Determine whether the user can restore the customers.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Customers  $customers
     * @return mixed
     */
    public function restore(User $user, Customers $customers)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the customers.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Customers  $customers
     * @return mixed
     */
    public function forceDelete(User $user, Customers $customers)
    {
        //
    }
}
