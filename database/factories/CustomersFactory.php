<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Customers::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'health_insurance_card'=>$faker->name,
        'birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'sex' => 'male',
        'address' => $faker->address,
        'phone' => $faker->e164PhoneNumber,
        'personal_id' => $faker->ean8,
    ];
});
