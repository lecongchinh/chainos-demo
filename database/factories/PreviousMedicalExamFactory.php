<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\PreviousMedicalExam::class, function (Faker $faker) {
    return [
        'medical_history' => $faker->name,
        'symptom' => $faker->name,
        'customer_id' => $faker->numberBetween($min = 1, $max = 50)
    ];
});
