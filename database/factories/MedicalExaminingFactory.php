<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\MedicalExamining::class, function (Faker $faker) {
    return [
        'symptom' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'indications_analysis' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'results_analysis' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'patient_monitoring_situation' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'daily_medication'  => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'customer_id' => $faker->numberBetween($min = 1, $max = 50)
    ];
});
