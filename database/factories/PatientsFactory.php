<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Patients;
use Faker\Generator as Faker;

$factory->define(App\Models\Patients::class, function (Faker $faker) {
    return [
        'status_patients_id' => '3',
        'fullname' => $faker->name,
        'sex' => 'nam',
        'age' => '30',
        'post_code' => '100000',
        'phone' => '0399494014',
        'address' => 'Nghe An',
    ];
});
