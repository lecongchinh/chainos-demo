<?php

use Illuminate\Database\Seeder;
// use DB;

class StatusPatientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_patients')->insert([
            'status' => 'Khám xong'
        ]);
    }
}
