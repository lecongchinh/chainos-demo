<?php

use Illuminate\Database\Seeder;

class MedicalExaminingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\MedicalExamining::class, 50)->create();
    }
}
