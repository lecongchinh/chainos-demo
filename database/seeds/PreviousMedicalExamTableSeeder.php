<?php

use Illuminate\Database\Seeder;

class PreviousMedicalExamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\PreviousMedicalExam::class, 50)->create();
    }
}
