@extends('layouts.app')

@section('content')
<div class="container">

    @if (session('warning'))
        <div class="alert alert-danger">
            {{ session('warning') }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <button type="button" class="btn btn-warning"><a href="/admin/customers">Warning</a></button>
            <button type="button" class="btn btn-primary">Primary</button>
            <button type="button" class="btn btn-secondary">Secondary</button>
            <button type="button" class="btn btn-success">Success</button>
        </div>
    </div>
</div>
@endsection
