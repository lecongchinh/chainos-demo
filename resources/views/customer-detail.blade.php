<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{asset('js/notify.js')}}"></script>
    <script src="{{asset('js/admin.js')}}"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="customer-detail">
            <div class="header">
                <div class="row">
                    <div class="col-md-8 header-left">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="infor">
                                    <div>CMND: {{$customer->personal_id}}</div>
                                    <div>
                                        <span>Name: {{$customer->name}}</span>
                                        <span class="float-right">Sex: {{$customer->sex}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="birth">
                                    <div>
                                        <span>Phone: {{$customer->phone}}</span>
                                    </div>
                                    <div>
                                        <span>Birth: {{$customer->birth}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 header-right">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="health_insurance_card">
                                    <div>
                                        <span>Thẻ BHYT: </span>
                                    </div>
                                    <div>
                                        <span>{{$customer->health_insurance_card}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-default float-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="address">
                    <p>Address: {{$customer->address}}</p>
                </div>
            </div>
            <div class="content">
                @if (session('danger'))
                    <div class="alert alert-danger">
                        {{ session('danger') }}
                    </div>
                @endif
                <div>
                    <div class="form-group">
                        <div class="row medical-examining">
                            <div class="col-md-6">
                                <div>
                                    <div class="title">
                                        <span><b>Lần đang khám:</b></span>
                                        <span>
                                            @if(count($customer->medicalExamining) > 0)
                                                <button onclick="showModalEditMcalExam({{$customer->medicalExamining[0]->id}})" type="button" class="btn btn-warning">Sửa</button>
                                                <button onclick="deleteMedicalExamining({{$customer->medicalExamining[0]->id}})" type="button" class="btn btn-success">Hoàn thành</button>
                                            @else 
                                                <button onclick="showModalAddMcalExam({{$customer->id}})" type="button" class="btn btn-primary">Thêm</button>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="medical-examining-left">
                                        @if(count($customer->medicalExamining) == 0)
                                            <p>Chưa bắt đầu khám</p>
                                        @else
                                            <ul>
                                                <li>Triệu chứng: {{$customer->medicalExamining[0]->symptom}}</li>
                                                <li>Chỉ định xét nghiệm: {{$customer->medicalExamining[0]->indications_analysis}}</li>
                                                <li>Kết quả xét nghiệm: {{$customer->medicalExamining[0]->results_analysis}}</li>
                                                <li>Tình hĩnh theo dõi bệnh nhân: {{$customer->medicalExamining[0]->patient_monitoring_situation}}</li>
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <div class="title">
                                        <b>Thuốc dùng hàng ngày</b>
                                    </div>
                                    <div class="medical-examining-right">
                                        @if(count($customer->medicalExamining) == 0)
                                            <p>Chưa có thuốc</p>
                                        @else
                                            <div>
                                                {{$customer->medicalExamining[0]->daily_medication}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <!-- <div class="list_circle">
                                    <table class="table text-center">
                                        <tbody>
                                            <tr>
                                                <td><div class="el_circle showModal1">aaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="medical_history">
                        <b>Các lần khám trước: </b> 
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Medicial History</th>
                                    <th>Symptom</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($customer->previousMedicalExam) == 0)
                                    <tr>
                                        <td>Chưa có lịch sử khám</td>
                                    </tr>
                                @else
                                    @foreach($customer->previousMedicalExam as $preExam)
                                        <tr>
                                            <td>{{$preExam->medical_history}}</td>
                                            <td>{{$preExam->symptom}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- modal add new medical examining -->
    <div class="modal" data-id="" id="modalAddMedicalExamining">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <input type="hidden" class="getId">
                <div class="modal-body">
                    <form id="FormAddMedicalExamining">
                        <div class="form-group">
                            <label for="demo">Symptom:</label>
                            <input type="text" name="symptom"  class="form-control" placeholder="symptom" id="symptom">
                            <p class="text-danger help-block err_symptom" style="display:none"></p>
                        </div>
                        <div class="form-group">
                            <label for="demo">Indications analysis:</label>
                            <input type="text" class="form-control" placeholder="Indications analysis" id="indications_analysis" name="indications_analysis">
                            <p class="text-danger help-block err_indications_analysis" style="display:none"></p>
                        </div>
                        <div class="form-group">
                            <label for="demo">Results analysis:</label>
                            <input type="text" class="form-control" placeholder="Results analysis" id="results_analysis" name="results_analysis">
                            <p class="text-danger help-block err_results_analysis" style="display:none"></p>
                        </div>
                        <div class="form-group">
                            <label for="demo">Patient monitoring situation:</label>
                            <input type="text" class="form-control" placeholder="Patient monitoring situation" id="patient_monitoring_situation" name="patient_monitoring_situation">
                            <p class="text-danger help-block err_patient_monitoring_situation" style="display:none"></p>    
                        </div>
                        <div class="form-group">
                            <label for="demo">Daily medication:</label>
                            <input type="text" class="form-control" placeholder="Daily medicaion" id="daily_medication" name="daily_medication">
                            <p class="text-danger help-block err_daily_medication" style="display:none"></p>    
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" onclick="addNewMedicalExamining()" class="btn btn-primary" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal edit medical examining -->
    <div class="modal" id="modalEditMedicalExamining">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <input type="hidden" class="getId">
                <div class="modal-body">
                    <form id="FormEditMedicalExamining">
                        <div class="form-group">
                            <label for="demo">Symptom:</label>
                            <input type="text" name="symptom"  class="form-control" placeholder="symptom" id="symptom">
                            <p class="text-danger help-block err_symptom" style="display:none"></p>
                        </div>
                        <div class="form-group">
                            <label for="demo">Indications analysis:</label>
                            <input type="text" class="form-control" placeholder="Indications analysis" id="indications_analysis" name="indications_analysis">
                            <p class="text-danger help-block err_indications_analysis" style="display:none"></p>
                        </div>
                        <div class="form-group">
                            <label for="demo">Results analysis:</label>
                            <input type="text" class="form-control" placeholder="Results analysis" id="results_analysis" name="results_analysis">
                            <p class="text-danger help-block err_results_analysis" style="display:none"></p>
                        </div>
                        <div class="form-group">
                            <label for="demo">Patient monitoring situation:</label>
                            <input type="text" class="form-control" placeholder="Patient monitoring situation" id="patient_monitoring_situation" name="patient_monitoring_situation">
                            <p class="text-danger help-block err_patient_monitoring_situation" style="display:none"></p>    
                        </div>
                        <div class="form-group">
                            <label for="demo">Daily medication:</label>
                            <input type="text" class="form-control" placeholder="Daily medicaion" id="daily_medication" name="daily_medication">
                            <p class="text-danger help-block err_daily_medication" style="display:none"></p>    
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button onclick="editMedicalExamining()" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</body>
</html>