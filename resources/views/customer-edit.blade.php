<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/clients.js')}}"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="customer-edit">
            <div class="header">
                <div class="row">
                    <div class="col-md-8 title">
                        <h1>Lê Công Chính</h1>
                    </div>
                    <div class="col-md-4 button">
                        <button type="submit" form="customers_edit" class="btn btn-default float-right">Save</button>
                    </div>
                </div>
            </div>
            <div class="body">
                <form method="POST" id="customers_edit" action="/admin/customer/edit/{{$customer->id}}">
                    @csrf
                    <div class="row row-parent">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <label class="highlight" for="name">test</label>
                                        <input name="name" type="text" value="{{$customer->name}}" class="form-control" id="name">
                                        {!! $errors->first('name', '<p class="text-danger help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sex">Giới tính</label>
                                        <label class="highlight" for="sex">test</label>
                                        <div id="sex">
                                            <div class="form-check form-check-inline">
                                                <input <?php if($customer->sex == 'male') echo 'checked' ?> class="form-check-input" type="radio" name="sex" id="male" value="male">
                                                <label class="form-check-label" for="male">Nam</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input <?php if($customer->sex == 'female') echo 'checked' ?> class="form-check-input" type="radio" name="sex" id="female" value="female">
                                                <label class="form-check-label" for="female">Nữ</label>
                                            </div>
                                            {!! $errors->first('sex', '<p class="text-danger help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="">
                                        <div class="form-group">
                                            <label for="age">Năm sinh</label>
                                            <label class="highlight" for="age">test</label>
                                            <div id="age" name="date">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-10 year">
                                                                <select name="year" class="form-control">
                                                                    @for($year=1900; $year<=2100; $year++)
                                                                        <option <?php if(date("Y",strtotime($customer->birth))==$year) echo 'selected' ?> value="{{$year}}">{{$year}}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 edge">Năm</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-10 month">
                                                                <select name="month" class="form-control">
                                                                    @for($month=1; $month<=12; $month++)
                                                                        <option <?php if(date("m",strtotime($customer->birth))==$month) echo 'selected' ?> value="{{$month}}">{{$month}}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 edge">Tháng</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-10 day">
                                                                <select name="day" class="form-control">
                                                                    @for($day=1; $day<=31; $day++)
                                                                        <option <?php if(date("d",strtotime($customer->birth))==$day) echo 'selected' ?> value="{{$day}}">{{$day}}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 edge">Ngày</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! $errors->first('date', '<p class="text-danger help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row row-parent">
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="health_insurance_card" class="col-md-4 col-form-label">Health Insurance Card</label>
                                <div class="col-md-8">
                                    <input name="health_insurance_card" value="{{$customer->health_insurance_card}}" class="form-control" id="health_insurance_card" placeholder="Health Insurance Card">
                                    {!! $errors->first('health_insurance_card', '<p class="text-danger help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row row-parent">
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="personal_id" class="col-md-4 col-form-label">Pesonal ID</label>
                                <div class="col-md-8">
                                    <input disabled value="{{$customer->personal_id}}" class="form-control" id="personal_id" placeholder="People ID">
                                    {!! $errors->first('personal_id', '<p class="text-danger help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row row-parent">
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                                        <div class="col-sm-9">
                                            <input name="phone" value="{{$customer->phone}}" class="form-control" id="phone" placeholder="Phone">
                                            {!! $errors->first('phone', '<p class="text-danger help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row row-parent">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="address"></label>
                                        <div id="address">Địa chỉ</div>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <label for="address">Địa chỉ</label>
                                        <input type="text" value="{{$customer->address}}" name="address" class="form-control" id="address" placeholder="Địa chỉ">
                                        {!! $errors->first('address', '<p class="text-danger help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>