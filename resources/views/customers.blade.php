@extends('layouts.app')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('danger'))
        <div class="alert alert-danger">
            {{ session('danger') }}
        </div>
    @endif
    <div class="row customers">
        <div class="col-md-3">
            <form method="POST" action='/admin/customers'>
                @csrf
                <div class="form-group">
                    <label for="id">Personal_id:</label>
                    <input type="text" name="personal_id" class="form-control" value="{{$request->personal_id}}" id="personal_id">
                </div>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" class="form-control" value="{{$request->name}}" id="name">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-9">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">NAME</th>
                        <th scope="col">Personal ID</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody class="">
                    @foreach($customers as $customer)
                        <tr>
                            <th scope="row">{{$customer->id}}</th>
                            <td><a href="/admin/customer/{{$customer->id}}">{{$customer->name}}</a></td>
                            <td>{{$customer->personal_id}}</td>
                            <td>
                                <button class="btn btn-warning"><a href="/admin/customer/edit/{{$customer->id}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></button>
                            </td>
                            <td>
                                <form method="post" action="/admin/customer/delete/{{$customer->id}}">
                                    {{csrf_field()}}
                                    @method('delete')
                                    <button class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
            {{$customers->links()}}
        </div>
    </div>
@endsection