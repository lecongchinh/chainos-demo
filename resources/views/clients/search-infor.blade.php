@extends('clients.layouts.app')

@section('search-infor')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="row search-infor">
        <div class="col-md-3">
            <form method="POST" action='/admin/search-infor'>
                @csrf
                <div class="form-group">
                    <label for="id">ID:</label>
                    <input type="text" name="id" class="form-control" value="{{$request->id}}" id="id">
                </div>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="fullname" class="form-control" value="{{$request->fullname}}" id="name">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-9">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">NAME</th>
                        <th scope="col">STATUS</th>
                        <th scope="col">TIME</th>
                    </tr>
                </thead>
                <tbody class="patients">
                    @foreach($patients as $patient)
                        <tr>
                            <th scope="row">{{$patient->id}}</th>
                            <td><a href="/">{{$patient->fullname}}</a></td>
                            <td>
                                <select class="form-control status-patients" data-id="{{$patient->id}}">
                                    @foreach($status_patients as $status)
                                        <option {{($patient->status_patients_id==$status->id) ? 'selected':''}} value="{{$status->id}}">{{$status->status}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>{{$patient->created_at}}</td>
                        </tr>
                    @endforeach
                </tbody>
                
            </table>
            {{$patients->links()}}
        </div>
    </div>
@endsection