<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/clients.css')}}">
    


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{asset('js/clients.js')}}"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="patient-detail">
            <div class="header">
                <div class="row">
                    <div class="col-md-8 header-left">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="infor">
                                    <p>Lee Cong Chinh</p>
                                    <p>aa aaaaaaaaaaaaa aa</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="date_now">
                                    <p>Lee Cong Chinh</p>
                                    <p>aa aaaaaaaaaaaaa aa</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 header-right">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="age">
                                    <p>Lee Cong Chinh</p>
                                    <p>aa aaaaaaaaaaaaa aa</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-default float-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div>
                    <div class="form-group">
                        <div class="age" name="date">
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="row">
                                        <div class="col-md-2 edge">Năm</div>
                                        <div class="col-md-10 year">
                                            <select name="year" class="form-control">
                                                @for($year=1900; $year<=2100; $year++)
                                                    <option <?php if(date('Y')==$year) echo 'selected' ?> value="{{$year}}">{{$year}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="row">
                                        <div class="col-md-2 edge">Tháng</div>
                                        <div class="col-md-10 month">
                                            <select name="month" class="form-control">
                                                @for($month=1; $month<=12; $month++)
                                                    <option <?php if(date('m')==$month) echo 'selected' ?> value="{{$month}}">{{$month}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 date_time_picker">
                                    <div class="row">
                                        <div class="col-md-2 edge">Ngày</div>
                                        <div class="col-md-10 day">
                                            <div class="row">
                                                <div class="col-sm-6" style="height:130px;">
                                                    <div class="form-group">
                                                        <div class='input-group date' id='datetimepicker11'>
                                                            <input type='text' class="form-control" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar">
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $(function () {
                                                        $('#datetimepicker11').datetimepicker({
                                                            daysOfWeekDisabled: [0, 6]
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! $errors->first('date', '<p class="text-danger help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <span>aaaaaaaaaaaa</span>
                                </div>
                                <div>
                                    aaaaaaaaaaaaaaa
                                </div>
                                <div>
                                    <p>aaaaaaa</p>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>4</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">aa</th>
                                                <td>aa</td>
                                                <td>aa</td>
                                                <td>aa</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">aa</th>
                                                <td>aa</td>
                                                <td>aa</td>
                                                <td>aa</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">aa</th>
                                                <td>aa</td>
                                                <td>aa</td>
                                                <td>aa</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div>
                                            <span>aaaaaaaaaaaaaaa</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div>
                                            <span>aaaaaaaaaa</span>
                                        </div>
                                    </div>
                                </div>
                                <div>aaaaaaa</div>
                                <div class="list_circle">
                                    <table class="table text-center">
                                        <tbody>
                                            <tr>
                                                <td><div class="el_circle showModal1">aaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                            </tr>
                                            <tr>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                                <td><div class="el_circle">aaaaaaa</div></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- show Modal 1 -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row">
                    <div class="col-md-5">
                        a
                    </div>
                    <div class="col-md-7">a</div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>