<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/clients.css')}}">

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="view2">
            <div class="header">
                <div class="row">
                    <div class="col-md-8 title">
                        <h1>Lê Công Chính</h1>
                    </div>
                    <div class="col-md-4 button">
                        <div class="row">
                            <div class='col-md-6'>
                                <button class="btn btn-default float-right"><a href="/view1">LCC</a></button>
                            </div>
                            <div class='col-md-6'>
                                <button class="btn btn-default float-right"><a href="/view2">LCC</a></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="tuts">
                    <b>Bạn hãy đánh dấu <i class="fa fa-check-square-o" aria-hidden="true"></i> vào câu trả lời đúng</b>
                </div>
                <div class="cau_1_den_3">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="cau_1">
                                <div class="question">
                                    <h6>1. Abcd abcd</h6>
                                </div>
                                <div class="answers">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                        <label class="form-check-label" for="inlineCheckbox1">1</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                        <label class="form-check-label" for="inlineCheckbox2">2</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
                                        <label class="form-check-label" for="inlineCheckbox3">3 </label>
                                    </div>
                                </div>
                            </div>
                            <div class="cau_2">
                                <div class="question">
                                    <h6>2. Cau 2</h6>
                                </div>
                                <div class="answers">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="cau_3">
                                <div class="question">
                                    <h6>3. Cau 3</h6>
                                </div>
                                <div class="answers">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cau_4_den_7">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="cau_4">
                                <div class="question">
                                    <h6>4. Cau 4</h6>
                                </div>
                                <div class="answers">
                                    <img src="{{asset('images/road-1072823__340.jpg')}}" width="100%" height="100%" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="cau_5">
                                <div class="question">
                                    <h6>5. Cau 5</h6>
                                </div>
                                <div class="answers">
                                    <b>Moi ban tra loi cau hoi ti day</b>
                                    <br/>
                                    <textarea name="" id="" cols="100" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="cau_6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="question">
                                            <h6>6. Cau 6</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="answers">
                                            <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                            <label class="checkbox-inline check-right"><input type="checkbox" value="">Option 2</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cau_8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="question">
                                            <h6>8. Cau 8</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="answers">
                                            <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                            <label class="checkbox-inline check-right"><input type="checkbox" value="">Option 2</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cau_9">
                                <div class="question">
                                    <h6>9. Cau 9</h6>
                                </div>
                                <div class="answers">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox check-right">
                                                        <label><input type="checkbox" value="">Option 1</label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>