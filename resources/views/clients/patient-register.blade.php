<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/clients.css')}}">
    


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/clients.js')}}"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="patient-register">
            <div class="header">
                <div class="row">
                    <div class="col-md-8 title">
                        <h1>Lê Công Chính</h1>
                    </div>
                    <div class="col-md-4 button">
                        <button type="submit" form="patients_register" class="btn btn-default float-right">Save</button>
                    </div>
                </div>
            </div>
            <div class="body">
                <form method="POST" id="patients_register" action="/patient/register">
                    @csrf
                    <div class="row row-parent">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="fullname">Full name</label>
                                        <label class="highlight" for="fullname">test</label>
                                        <input name="fullname" type="text" class="form-control" id="fullname">
                                        {!! $errors->first('fullname', '<p class="text-danger help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sex">Giới tính</label>
                                        <label class="highlight" for="sex">test</label>
                                        <div id="sex">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="sex" id="male" value="male">
                                                <label class="form-check-label" for="male">Nam</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="sex" id="female" value="female">
                                                <label class="form-check-label" for="female">Nữ</label>
                                            </div>
                                            {!! $errors->first('sex', '<p class="text-danger help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="">
                                        <div class="form-group">
                                            <label for="age">Năm sinh</label>
                                            <label class="highlight" for="age">test</label>
                                            <div id="age" name="date">
                                                <div class="row">
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-10 year">
                                                                <select name="year" class="form-control">
                                                                    @for($year=1900; $year<=2100; $year++)
                                                                        <option <?php if(date('Y')==$year) echo 'selected' ?> value="{{$year}}">{{$year}}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 edge">Năm</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-10 month">
                                                                <select name="month" class="form-control">
                                                                    @for($month=1; $month<=12; $month++)
                                                                        <option <?php if(date('m')==$month) echo 'selected' ?> value="{{$month}}">{{$month}}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 edge">Tháng</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-10 day">
                                                                <select name="day" class="form-control">
                                                                    @for($day=1; $day<=31; $day++)
                                                                        <option <?php if(date('d')==$day) echo 'selected' ?> value="{{$day}}">{{$day}}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 edge">Ngày</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! $errors->first('date', '<p class="text-danger help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row row-parent">
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label for="post_code" class="col-md-4 col-form-label">Post Code</label>
                                <div class="col-md-8">
                                    <input name="post_code" class="form-control" id="post_code" placeholder="Post Code">
                                    {!! $errors->first('post_code', '<p class="text-danger help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row row-parent">
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                                        <div class="col-sm-9">
                                            <input name="phone" class="form-control" id="phone" placeholder="Phone">
                                            {!! $errors->first('phone', '<p class="text-danger help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row row-parent">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="address"></label>
                                        <div id="address">Địa chỉ</div>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <label for="address">Địa chỉ</label>
                                        <input type="text" name="address" class="form-control" id="address" placeholder="Địa chỉ">
                                        {!! $errors->first('address', '<p class="text-danger help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>