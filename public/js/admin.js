$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// $(document).ready(function() {
//     $('#modalAddMedicalExamining .modal-footer button').on('click', function() {
//         $.ajax({
//             method: 'post',
//             url: '/customer/' + id + '/medical-examining/create',
//             success: function() {
//                 alert(123);
//             }
//         })
//     })
// })

function deleteCustomer(id) {
    

    if (confirm('Are you sure you want to Delete this customer ?')) {
        $.ajax({
            method: "post", 
            url: '/admin/customer/delete/' + id,
        })
    } else {
        return false;
    }
}

function showModalAddMcalExam(id) {
    $('.err_symptom').hide();
    $('.err_indications_analysis').hide();
    $('.err_results_analysis').hide();
    $('.err_patient_monitoring_situation').hide();
    $('.err_daily_medication').hide();
    $('#modalAddMedicalExamining').modal('show');
    $('.getId').val(id);
}

function addNewMedicalExamining() {
    $('.err_symptom').hide();
    $('.err_indications_analysis').hide();
    $('.err_results_analysis').hide();
    $('.err_patient_monitoring_situation').hide();
    $('.err_daily_medication').hide();
    let id = $('.getId').val();
    $.ajax({
        method: 'post',
        url: '/admin/customer/' + id + '/medical-examining/create',
        data: $("#FormAddMedicalExamining").serialize(),
        
        success: function() {
            document.getElementById('FormAddMedicalExamining').reset();
            $('.getId').val('');
            $.notify("Add Complete", "success");
            location.reload();
        },
        error: function(data) {
            $('#modalAddMedicalExamining').modal('show');
            console.log(data.responseJSON.errors);
            $.each(data.responseJSON.errors, function(index, value) {
                $('.err_' + index + '').text(value);
                $('.err_' + index).show();
            })
            $.notify("Add Failed", "error");
        }
    })
}

function showModalEditMcalExam(id) {
    $('.err_symptom').hide();
    $('.err_indications_analysis').hide();
    $('.err_results_analysis').hide();
    $('.err_patient_monitoring_situation').hide();
    $('.err_daily_medication').hide();
    $('#modalEditMedicalExamining').modal('show');
    $('.getId').val(id);
    $.ajax({
        method: 'get',
        url: '/admin/customer/medical-examining/' + id,
        success: function(data) {
            $('#FormEditMedicalExamining #symptom').val(data[0]['symptom']);
            $('#FormEditMedicalExamining #indications_analysis').val(data[0]['indications_analysis']);
            $('#FormEditMedicalExamining #results_analysis').val(data[0]['results_analysis']);
            $('#FormEditMedicalExamining #patient_monitoring_situation').val(data[0]['patient_monitoring_situation']);
            $('#FormEditMedicalExamining #daily_medication').val(data[0]['daily_medication']);
            
        }
    })
}

function editMedicalExamining() {
    $('.err_symptom').hide();
    $('.err_indications_analysis').hide();
    $('.err_results_analysis').hide();
    $('.err_patient_monitoring_situation').hide();
    $('.err_daily_medication').hide();
    let id = $('.getId').val();
    $.ajax({
        method: 'post',
        url: '/admin/customer/medical-examining/'+ id+'/edit',
        data: $("#FormEditMedicalExamining").serialize(),
        
        success: function() {
            document.getElementById('FormEditMedicalExamining').reset();
            $('.getId').val('');
            $('#modalEditMedicalExamining').modal('hide');
            $.notify("Edit Complete", "success");
            location.reload();
        },
        error: function(data) {
            $('#modalEditMedicalExamining').modal('show');
            console.log(data.responseJSON.errors);
            $.each(data.responseJSON.errors, function(index, value) {
                $('.err_' + index + '').text(value);
                $('.err_' + index).show();
                $.notify("Edit Faile", "error");
            })
        }
    })
}

function deleteMedicalExamining(id) {
    if (confirm('Are you sure ?')) {
        $.ajax({
            method: "post", 
            url: '/admin/customer/medical-examining/'+id+'/delete',
            success: function() {
                location.reload();
                $.notify("Delete Complete", "success");
            },
            error: function() {
                alert('Xóa không thành công!')
            }
        })
    } else {
        return false;
    }
}