<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 
Route::get('/view1', function() {
    return view('clients.view1');
});

Route::get('/view2', function() {
    return view('clients.view2');
});

Route::prefix('/admin')->middleware('checkAdmin')->group(function() {
    Route::get('/customers', 'CustomersController@index');
    Route::post('/customers', 'CustomersController@index');


    Route::get('/customer/register', 'CustomersController@getCreate');
    Route::post('/customer/register', 'CustomersController@postCreate');

    Route::get('/customer/edit/{id}', 'CustomersController@getEdit');
    Route::post('/customer/edit/{id}', 'CustomersController@postEdit');

    Route::delete('/customer/delete/{id}', 'CustomersController@deleteCustomer');

    Route::get('/customer/{id}', 'CustomersController@showDetailCustomer')->name('customer-detail');

    Route::post('/customer/{id}/medical-examining/create', 'MedicalExaminingController@create');
    Route::post('/customer/medical-examining/{id}/edit', 'MedicalExaminingController@update');
    Route::post('/customer/medical-examining/{id}/delete', 'MedicalExaminingController@delete');

    Route::get('/customer/medical-examining/{id}', 'MedicalExaminingController@getById');
    
    // Route::get('/search-infor/search', 'Clients\InforPatientsController@getSearch');
    Route::post('/search-infor', 'Clients\InforPatientsController@index');

    Route::post('/search-infor/update-status/{id}', 'Clients\InforPatientsController@updateStatus');

});



Route::get('/patient/register', 'Clients\PatientsController@index');
Route::post('/patient/register', 'Clients\PatientsController@create');
Route::get('/patient/detail/{id}', 'Clients\PatientsController@show');

Route::get('locale/{locale}', 'LocaleController@index');